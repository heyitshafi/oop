<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$sheep = new animal("Shaun");

echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>";

$frog = new Frog("Buduk");

echo "Name : " . $frog->name . "<br>";
echo "Legs : " . $frog->legs . "<br>";
echo "Cold Blooded : " . $frog->cold_blooded . "<br>";
echo $frog->jump() . "<br><br>";

$ape = new Ape("Kera Sakti");

echo "Name : " . $ape->name . "<br>";
echo "Legs : " . $ape->legs . "<br>";
echo "Cold Blooded : " . $ape->cold_blooded . "<br>";
echo $ape->yell();


?>